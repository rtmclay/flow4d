! -*- f90 -*-
#include "assert.hf"
program main
   use mpi
   use hdf5

   implicit none

   TYPE BlockDataType
      INTEGER                                    :: numberOfXVariables, numberOfQVariables
      INTEGER(HSIZE_t)                           :: xOffset,            qOffset
      INTEGER, DIMENSION(4)                      :: iMax       ! array bounds
      REAL*8,  DIMENSION(:,:,:,:,:),ALLOCATABLE  :: xVec, qVec ! limited to 4D array bounds,
                                                               ! with the 5th dimension being
                                                               ! the variable ID
   END TYPE BlockDatatype

   TYPE(BlockDataType), DIMENSION(:), ALLOCATABLE :: blockDataArray

   character(80)      :: fn
   integer, parameter :: gridDim = 3
   integer, parameter :: gridSz  = 5
   integer, parameter :: solnDim = 5
   integer, parameter :: N       = 3
   integer            :: i,j,k,l,iblk, blkSz, xStart, qStart, nDim
   integer(hid_t)     :: file_id      ! File    identifier
   integer(hid_t)     :: group_id     ! Group   identifier
   integer(hid_t)     :: dset_id      ! Dataset identifier
   integer(hid_t)     :: plist_id     ! Property list identifier
   integer(hid_t)     :: filespace    ! Dataspace identifier in file
   integer(hid_t)     :: memspace     ! Dataspace identifier in memory
   integer(HSIZE_t)   :: sz(1), gsz(1), starts(1)
   integer            :: ierr, comm, myProc, nProc, info
   character(40)      :: descript


   comm = MPI_COMM_WORLD
   call MPI_init(ierr)
   call MPI_Comm_size(comm, nProc, ierr);  ASSERT(ierr == 0, "MPI_Comm_size")
   call MPI_Comm_rank(comm, myProc, ierr); ASSERT(ierr == 0, "MPI_Comm_rank")
   

   descript = "4-D soln vector"

   ! Allocate block and initialize N bricks

   fn = 'flow4d.h5'


   allocate (blockDataArray(N))

   nDim  = 1

   ! compute the offset for each task
   ! Here we are assuming that each task has the same number of bricks with the same size

   
   blkSz  = gridSz * gridSz * gridSz * gridSz

   xStart = blkSz*N*myProc*gridDim
   qStart = blkSz*N*myProc*solnDim

   do iblk = 1, N
      blockDataArray(iblk) % xOffset            = xStart
      blockDataArray(iblk) % qOffset            = qStart
      blockDataArray(iblk) % numberOfXVariables = gridDim
      blockDataArray(iblk) % numberOfQVariables = solnDim
      do i = 1,4
         blockDataArray(iblk) % iMax(i) = gridSz
      enddo
      allocate (blockDataArray(iblk) % xVec(gridSz, gridSz, gridSz, gridSz, gridDim))
      allocate (blockDataArray(iblk) % qVec(gridSz, gridSz, gridSz, gridSz, solnDim))

      blockDataArray(iblk) % xVec = 1.0
      blockDataArray(iblk) % qVec = real(iblk)

      xStart = xStart + blkSz*gridSz
      qStart = qStart + blkSz*solnDim
   enddo

   
   ! (3.2) Define global size for filespace
   gsz(1) = blkSz*solnDim*N*nProc
   
   ! (3.2) Define local size for memspace
   sz(1)  = blkSz*solnDim

   !
   ! (1) Open HDF5
   info = MPI_INFO_NULL
   call MPI_Info_create(info, ierr)

   call H5open_f(ierr); ASSERT(ierr == 0, "H5Open_f")

   !
   ! (2) Setup file access property list w/ parallel I/O access.

   call H5Pcreate_f(H5P_FILE_ACCESS_F,plist_id,ierr)
   ASSERT(ierr == 0, "H5Pcreate_f")
   call H5Pset_fapl_mpio_f(plist_id, comm, info, ierr);
   ASSERT(ierr == 0, "H5Pset_fapl_mpio_f")

   !
   ! (3.0) Create the file collectively
   !t0 = walltime()
   call H5Fcreate_f(fn, H5F_ACC_TRUNC_F, file_id, ierr, access_prp = plist_id)
   ASSERT(ierr == 0, "H5fcreate_f")

   call H5Pclose_f(plist_id, ierr)
   ASSERT(ierr == 0, "H5Pclose_f")


   ! (3.1) Create group
   call H5Gcreate_f(file_id,"Solution", group_id, ierr)
   ASSERT(ierr == 0, "H5Gopen_f")

   
   ! Add "fake" solution description
   call add_solution_description(group_id)

   !
   ! (4) Create the data space for the dataset: filespace, memspace
   call H5Screate_simple_f(nDim, gsz, filespace, ierr)
   ASSERT(ierr == 0, "H5Screate_simple_f")
   !
   ! Each process defines dataset in memory and writes it to the hyperslab
   ! in the file.
   call H5Screate_simple_f(nDim, sz, memspace, ierr)
   ASSERT(ierr == 0, "H5Screate_simple_f")
   

   !
   ! (7a) Create property list for collective dataset write
   call H5Pcreate_f(H5P_DATASET_XFER_F, plist_id, ierr)
   ASSERT(ierr == 0, "H5Pcreate_f")
   call H5Pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierr)
   ASSERT(ierr == 0, "H5Pset_dxpl_mpio_f")

   ! (5) Create the data set
   CALL H5Dcreate_f(group_id, "soln", H5T_NATIVE_DOUBLE, filespace, &
        dset_id, ierr)
   ASSERT(ierr == 0, "H5Dcreate_f")
   
   CALL H5Sclose_f(filespace, ierr)
   ASSERT(ierr == 0, "H5Sclose_f")

   call add_attribute(dset_id, descript)

   do iblk = 1, N

      starts(1) = blockDataArray(iblk) % qOffset

      ! (4) Create the data space for the dataset: filespace, memspace
      call H5Screate_simple_f(nDim, gsz, filespace, ierr)
      ASSERT(ierr == 0, "H5Screate_simple_f")

      CALL H5Dget_space_f(dset_id, filespace, ierr)

      CALL H5Sselect_hyperslab_f (filespace, H5S_SELECT_SET_F, starts, sz, ierr)
      ASSERT(ierr == 0, "H5select_hyperslab_f")
      
      !
      ! (8) Write the dataset collectively.

      print *,"iblk:",iblk,", sz: ",sz(1),", gsz: ",gsz(1),", starts: ",starts(1)

      call H5Dwrite_f(dset_id, H5T_NATIVE_DOUBLE, blockDataArray(iblk) % qVec, gsz, ierr, &
           file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
      ASSERT(ierr == 0, "H5Dwrite_f")

      !
      ! (9) Close dataspaces.
      !
      call H5Sclose_f(filespace, ierr); ASSERT(ierr == 0,"H5Sclose_f")
   enddo


   call H5Sclose_f(memspace,  ierr); ASSERT(ierr == 0,"H5Sclose_f")
   !
   ! (10) Close the dataset and property list.
   !
   call H5Dclose_f(dset_id,  ierr); ASSERT(ierr == 0,"H5Dclose_f")
   call H5Pclose_f(plist_id, ierr); ASSERT(ierr == 0,"H5Pclose_f")

   !
   ! (12) Close the group and file.
   !
   call H5Gclose_f(group_id, ierr); ASSERT(ierr == 0,"H5Gclose_f")
   call H5Fclose_f(file_id,  ierr); ASSERT(ierr == 0,"H5Fclose_f")
   
   !
   ! (12) Close FORTRAN predefined datatypes.
   !
   call H5Close_f(ierr); ASSERT(ierr == 0,"H5Close_f")

   call MPI_Finalize(ierr);

end program main
