! -*- f90 -*-
module cmdline
   use mpi
   implicit none
   logical         :: Debug
   logical         :: JsonOutput
   integer         :: usrGridSz    ! Minimum grid size in one-D (5-800)
   integer         :: NblksPerTask ! Minimum number of blocks per MPI task
   logical         :: HelpFlag
   character*(160) :: Outputdir
   character*(160) :: H5_fn

contains

   subroutine parse(myProc)
      integer        :: count, i, iargc, ierr, myProc
      character(160) :: arg, optarg
      
      count = iargc()

      OutputDir    = "./"
      HelpFlag     = .false.
      Debug        = .false.
      JsonOutput   = .false.
      H5_fn        = "FLOW4D.h5"
      usrGridSz    = 4
      NblksPerTask = 2
   
      i = 0
      do
         i = i + 1
         if (i > count) exit
         call getarg(i,arg)

         if (arg(1:2) == '-H' .or. arg(1:2) == '-h' .or. &
              arg == '--help') then
            HelpFlag = .true.

         elseif (arg == "--json") then
            JsonOutput = .true.
         
         elseif (arg == "-d" .or. arg == "--debug") then
            Debug = .true.

         elseif (arg == "-g" .or. arg(1:4) == "--grid") then
            i = i + 1
            call getarg (i, optarg)
            read(optarg,*, err = 11) usrGridSz
         elseif (arg == "-o" .or. arg == "--outputDir") then
            i = i + 1
            call getarg (i, optarg)
            OutputDir = optarg
         elseif (arg == "-f" .or. arg == "--fn") then
            i = i + 1
            call getarg (i, optarg)
            H5_fn = optarg
         elseif (arg == "-N" .or. arg(1:4) == "--nblk") then
            i = i + 1
            call getarg (i, optarg)
            read(optarg,*, err = 11) NblksPerTask
         endif
      end do
      return 

11    continue
      if (myProc == 0) then
         print *, "Illegal argument for option: ", trim(arg)
         print *, "Terminating."
      end if
      call MPI_Finalize(ierr)
      call exit()
   end subroutine parse

   subroutine usage()
      implicit none

      print *, "Usage:  [options]"
      print *, "options:"
      print *, "  -H, --help        : This message and quit"
      print *, "  -N num            : min. number of blks per mpi task (default 2)"
      print *, "  -g num            : min. number of points in each direction per block (default 4)"
      print *, "  --json            : json output of performance (default false)"
      print *, "  --outputDir path  : Path to output directory (default './')"
      print *, "  --fn file.h5      : Name of h5 output file (default 'FLOW4D.h5')"

      print *, " "
      print *, " Defaults are:"
      print *, "    N    is 2"
      print *, "    grid is 5"
      print *, " "

   end subroutine usage
         
end module cmdline
