! -*- f90 -*-
#include "assert.hf"
module blocks
   use hdf5
   use mpi
   integer, parameter :: NDIM = 1

   type BlockDataType
      integer                                    :: numberOfXVariables, numberOfQVariables
      integer(hsize_t)                           :: xOffset,            qOffset
      integer                                    :: gridSz     ! number of points in 1 direction.
      integer, dimension(4)                      :: iMax       ! array bounds
      real*8,  dimension(:,:,:,:,:),allocatable  :: xVec, qVec ! limited to 4D array bounds,
                                                               ! with the 5th dimension being
                                                               ! the variable ID
   end type BlockDatatype

   type grid_t
      integer   :: N         ! number of blocks locally
      integer   :: maxN      ! max number of blocks locally
      integer*8 :: totalSz   ! total number of grid points across all blocks
      integer*8 :: blkSz     ! number of grid points per block.
                             ! Here we assume that blkSz is uniform on all blocks on this task
                             ! Can be different between tasks.
      integer*8 :: numBytesG ! global soln vector size in bytes
      integer*8 :: numBytesL ! Maximum Size of local soln in bytes
   end type grid_t


contains
   subroutine createBlks(gridSz, N, gridDim, solnDim, comm, myProc, nProc,blockDataArray, grid)

      implicit none
      type(BlockDataType), dimension(:), allocatable :: blockDataArray

      type(grid_t)         :: grid
      integer              :: comm, gridSz, N, ierr
      integer              :: gridDim, solnDim
      integer              :: myProc, nProc, i, iblk
      integer              :: startBlk, myStart, totalBlks
      integer(hsize_t)     :: xstart, qStart
      integer, allocatable :: nblkA(:,:)

      !------------------------------------------------------------
      ! compute size information for blocks

      allocate(nblkA(0:nProc-1,2))

      nblkA = 0
      nblkA(myProc,1) = N
      nblkA(myProc,2) = gridSz * gridSz * gridSz * gridSz
      call MPI_Allreduce(MPI_IN_PLACE, nblkA, nProc*2, MPI_INTEGER, MPI_SUM, comm, ierr)
      ASSERT(ierr == 0, "MPI_Allreduce")

      grid % numBytesL = 0
      startBlk         = 0
      myStart          = 0
      grid % maxN      = 0
      grid % totalSz   = 0
      do i = 0, myProc-1
         startBlk         = startBlk + nblkA(i,1)
         myStart          = myStart  + nblkA(i,2) * nblkA(i,1)
         grid % maxN      = max(grid % maxN,  nblkA(i,1))
         grid % numBytesL = max(grid % numBytesL, nblkA(i,2) * nblkA(i,1) * solnDim * 8)
      enddo
      totalBlks      = startBlk
      grid % totalSz = myStart
      do i = myProc, nProc-1
         totalBlks        = totalBlks + nblkA(i,1)
         grid % totalSz   = grid % totalSz   + nblkA(i,2) * nblkA(i,1)
         grid % maxN      = max(grid % maxN, nblkA(i,1))
         grid % numBytesL = max(grid % numBytesL, nblkA(i,2) * nblkA(i,1) * solnDim * 8)
      enddo

      grid % numBytesG    = grid % totalSz * solnDim * 8

      grid % blkSz = nblkA(myProc,2)
      grid % N     = N

      deallocate(nblkA)

      xStart = myStart*gridDim
      qStart = myStart*solnDim
      do iblk = 1, grid % N
         blockDataArray(iblk) % xOffset            = xStart
         blockDataArray(iblk) % qOffset            = qStart
         blockDataArray(iblk) % gridSz             = gridSz
         blockDataArray(iblk) % numberOfXVariables = gridDim
         blockDataArray(iblk) % numberOfQVariables = solnDim
         do i = 1,4
            blockDataArray(iblk) % iMax(i) = gridSz
         enddo

         allocate (blockDataArray(iblk) % qVec(gridSz, gridSz, gridSz, gridSz, solnDim))

         xStart = xStart + grid % blkSz*gridSz
         qStart = qStart + grid % blkSz*solnDim
      end do


      !------------------------------------------------------------
      ! Initial block

      do iblk = 1, grid % N
         blockDataArray(iblk) % qVec = real(startBlk + iblk)
      enddo

   end subroutine createBlks
   
end module blocks
