all:
	cd ./src; make

world_update:
	git status -s > /tmp/git_st_$$$$                                     		      ; \
        if [ -s /tmp/git_st_$$$$ ]; then                                      		        \
            echo "All files not checked in => try again"                      		      ; \
        else                                                                  		        \
	    branchName=`git status | head -n 1 | sed 's/^[# ]*On branch //g'` 		      ; \
            git push        bitbucket     $$branchName                           		      ; \
            git push --tags bitbucket     $$branchName                           		      ; \
        fi                                                                    		      ; \
        rm -f /tmp/git_st_$$$$
