#!/usr/bin/env lua
-- -*- lua -*-

Version = "0.0"

require("strict")
require("string_utils")
require("VarDump")
local Optiks = require("Optiks")
local dbg    = require("Dbg"):dbg()
local master = {}

function masterTbl()
   return master
end

function main()
   options()
   local masterTbl   = masterTbl()
   
   local solnDim = 5
   local nProcs = tonumber(masterTbl.procs)
   local gridSz = tonumber(masterTbl.gridSz)
   local nblks  = tonumber(masterTbl.nblks)

   local gbyte     = 1024.0*1024.0*1024.0
   local localMem  = 8.0*gridSz*gridSz*gridSz*gridSz*solnDim*nblks/gbyte
   local globalMem = nProcs*localMem

   print("localMem: ",localMem, "globalMem: ",globalMem)
   
end

function options()
   local masterTbl = masterTbl()
   local usage         = "Usage: "
   local cmdlineParser = Optiks:new{usage=usage, version=Version}

   cmdlineParser:add_option{ 
      name   = {'-v','--verbose'},
      dest   = 'verbosityLevel',
      action = 'count',
   }

   cmdlineParser:add_option{ 
      name   = {'-p'},
      dest   = 'procs',
      action = 'store',
      default = "1"
   }

   cmdlineParser:add_option{ 
      name   = {'-g'},
      dest   = 'gridSz',
      action = 'store',
   }

   cmdlineParser:add_option{ 
      name   = {'-N'},
      dest   = 'nblks',
      action = 'store',
      default = "1"
   }
   local optionTbl, pargs = cmdlineParser:parse(arg)

   for v in pairs(optionTbl) do
      masterTbl[v] = optionTbl[v]
   end
   masterTbl.pargs = pargs

end

main()
